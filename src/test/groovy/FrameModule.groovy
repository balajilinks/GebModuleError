
import geb.Module


class FrameModule extends Module {

    static content = {
        bannerFrame { $("frame", name: 'topNav') }
        menuFrame { $("frame", name: 'menu') }
        contentFrame { $("frame", name: 'content') }
//        navigationFrame { $("frame", name: 'nav') }
//        contentFrame { $("frame", name: 'content') }

        pageTitle {
            withFrame(bannerFrame) {
                $("h3").text()
            }
        }
    }

}