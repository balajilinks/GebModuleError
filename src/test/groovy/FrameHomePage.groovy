import geb.Page

class FrameHomePage extends Page {
    static at = { trial.pageTitle == "Top Navbar"  }

    static content = {
        trial { module FrameModule }
    }
}
